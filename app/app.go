package app

import (
	"gitlab.com/regidr0nn/mvc/controllers"
	"log"
	"net/http"
)

func StartApp() {
	http.HandleFunc("/users", controllers.GetUser) //возращает пользователей

	http.HandleFunc("/createUser", controllers.CreateUser)

	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	} //порт который нужен

}
