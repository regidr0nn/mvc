package controllers

import (
	"encoding/json"
	"gitlab.com/regidr0nn/mvc/domain"
	"gitlab.com/regidr0nn/mvc/services"
	"gitlab.com/regidr0nn/mvc/utils"
	"net/http"
	"os/user"
	"strconv"
)

var NewUser domain.User

func GetUser(w http.ResponseWriter, r *http.Request) {
	userId, err := strconv.ParseInt(r.URL.Query().Get("user_id"), 10, 64)
	/* is - library // make an int coz user_id will come as a string*/
	if err != nil {
		apiErr := &utils.ApplicationError{
			Message: "user_id must be a number",
			Status:  http.StatusBadRequest,
			Code:    "bad request",
		}
		jsonValue, _ := json.Marshal(apiErr) // ignoring _ error
		// json it is our error
		w.WriteHeader(apiErr.Status)
		w.Write(jsonValue)
		return
	}
	user, apiErr := services.GetUser(userId) //make 2 variables, send user_id, return user or err
	if apiErr != nil {                       //not ok
		jsonValue, _ := json.Marshal(apiErr)
		w.WriteHeader(apiErr.Status)
		w.Write(jsonValue)
		return
	}
	jsonValue, _ := json.Marshal(user)
	w.Write(jsonValue)
	return
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	CreateUser := &domain.User{}
	utils.ParseBody(r, CreateUser)
	u := CreateUser.CreateUser()
	res, _ := json.Marshal(u)
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}
