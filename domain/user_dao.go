package domain

import (
	"gitlab.com/regidr0nn/mvc/utils"
	"net/http"
)

// dao - data access object
var users = map[int64]*User{
	13: {
		Id:        13,
		FirstName: "Aliya",
		LastName:  "Izmailova",
		Email:     "regidr0nn@gmailcom",
	},
	12: {
		Id:        12,
		FirstName: "Asdf",
		LastName:  "Qwerty",
		Email:     "d.gim@gmail.com",
	},
}

func GetUser(userId int64) (*User, *utils.ApplicationError) { //функция либо юзера либо ошибка
	if user := users[userId]; user != nil {
		return user, nil
	}
	return nil, &utils.ApplicationError{
		Message: "user not found",
		Status:  http.StatusNotFound,
		Code:    "not_found",
	}
}
