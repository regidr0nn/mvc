package services

import (
	"gitlab.com/regidr0nn/mvc/domain"
	"gitlab.com/regidr0nn/mvc/utils"
)

func GetUser(userId int64) (*domain.User, *utils.ApplicationError) {
	return domain.GetUser(userId) //& нужен тогда когда в 1 раз создали обьект
}
